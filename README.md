lua-affinity-balancer-module
============================

Computes weight of Nginx upstream peers according to their availability and their availability zone.
By default only peers in the same availability zone will be used.
When some peers in the same availability zone are becoming unavailable, weight of unavailable peers (of the current availability zone) will be redistributed to all other available peers (even those in other availability zones).
If all peers are down, peers weights are reset to the default value.

Requirements
------------
Required packages:
- [lua-resty-balancer](https://github.com/openresty/lua-resty-balancer)
- [lua-resty-core](https://github.com/openresty/lua-resty-core.git)

Usage
-----

This module is intended to be used with [lua-resty-balancer](https://github.com/openresty/lua-resty-balancer) round robin load balancer.

Peers are loaded from the Nginx upstream server list.
Peers are considered in the same availability zone if their default weight (from the `server` Nginx directive) is greater than the total number of peers for the upstream.
Peers with lower weight are considered in another availability zone.

An example of `upstream` declaration:
```
    upstream my_upstream {
        server 1.1.1.1 weight=5;
        server 1.1.1.2 weight=5;
        server 1.1.2.1;
        server 1.1.2.2;

        balancer_by_lua_block {
            local b = require "ngx.balancer"
            local rr_up = package.loaded.my_rr_up
            local server = rr_up:find()
            assert(b.set_current_peer(server))
        }
        keepalive 1024;
    }
```

Configure Nginx lua paths with:
```
lua_package_path "/var/lib/nginx/balancer_module/lib/?.lua;;/var/lib/nginx/affinity_balancer_module/?.lua;;/var/lib/nginx/resty_core_module/lib/?.lua;;";
lua_package_cpath "/var/lib/nginx/balancer_module/?.so;;";
```

The round robin load balancer can be initialized/updated like this:
```
  local upstream = require "ngx.upstream"
  local resty_roundrobin = require "resty.roundrobin"
  local affinity = require "affinity_balancer"

  local peers = upstream.get_primary_peers("my_upstream")
  package.loaded.servers_list = affinity.update_weights(peers)
  package.loaded.my_rr_up = resty_roundrobin:new(package.loaded.servers_list)

```
