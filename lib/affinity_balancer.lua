local ok, upstream = pcall(require, "ngx.upstream")

local _M = {
  _VERSION = '0.01'
}

function _M.factorial(n)
  if n == 0 then
    return 1
  else
    return n * _M.factorial(n - 1)
  end
end

function gcd(a, b)
  if b == 0 then
    return a
  end

  return gcd(b, a % b)
end

function same_az_weight(peers_count, same_az_unavailable_peers)
  if same_az_unavailable_peers == 0 then
    return _M.factorial(peers_count - 1)
  else
    -- compute (recursively) weight with 1 less unavailable peers
    local weight_with_one_more_available_peers = same_az_weight(peers_count, same_az_unavailable_peers - 1)
    local unavailable_same_az_weight_peer_portion = weight_with_one_more_available_peers / (peers_count - same_az_unavailable_peers)
    -- add a portion of this weight to itself <=> distribute the weight of 1 unavailable peers to all available peers
    return weight_with_one_more_available_peers + unavailable_same_az_weight_peer_portion
  end
end

function other_az_weight(peers_count, same_az_unavailable_peers)
  if same_az_unavailable_peers == 0 then
    return 0
  else
    local weight_with_one_more_available_peers = other_az_weight(peers_count, same_az_unavailable_peers - 1)
    local unavailable_same_az_weight_peer_portion = same_az_weight(peers_count, same_az_unavailable_peers - 1) / (peers_count - same_az_unavailable_peers)
    return weight_with_one_more_available_peers + unavailable_same_az_weight_peer_portion
  end
end

function _M.update_weights(peers)
  local nb_peers = #peers
  local peers_down_in_current_az = 0
  local peers_in_current_az = 0
  local result = {}
  local all_peers_down = true
  local az_peers_down = {}
  local zones = {}
  local peers_down = 0
  local current_az_index = 0
  local current_zone = {
    pweight = 1,
    sweight = 0,
    peers_down = 0,
    nb_peers = 0
  }

  for i = 1, nb_peers do
    local peer = peers[i]
    local az_index = peer.weight

    if not zones[az_index] then
      zones[az_index] = {
        pweight = 0,
        sweight = 0,
        peers_down = 0,
        nb_peers = 0
      }
    end

    if az_index > nb_peers then
      current_az_index = az_index
      current_zone = zones[current_az_index]
    end

    zones[az_index].nb_peers = zones[az_index].nb_peers + 1

    if peer.down then
      if az_index > nb_peers then
        peers_down_in_current_az = peers_down_in_current_az + 1
      end
      zones[az_index].peers_down = zones[az_index].peers_down + 1
      peers_down = peers_down + 1
    else
      all_peers_down = false
    end
  end

  for az_index, zone in pairs(zones) do
    zone.pweight = same_az_weight(nb_peers, zone.peers_down)
    zone.sweight = other_az_weight(nb_peers, zone.peers_down)
  end

  local az_sweight = {}
  local weight_gcd = current_zone.pweight
  for az_index, zone in pairs(zones) do
    if az_index ~= current_az_index and current_zone.sweight == zone.sweight or az_index == current_az_index then
      az_sweight[az_index] = 0
    else
      az_sweight[az_index] = current_zone.sweight
    end
    weight_gcd = gcd(weight_gcd, az_sweight[az_index])
  end

  if all_peers_down then
    for i = 1, nb_peers do
      local peer = peers[i]
      local az_index = peer.weight
      if az_index == current_az_index or current_zone.nb_peers == 0 then
        result[peer.name] = 1
      else
        result[peer.name] = 0
      end
    end
  else
    for i = 1, nb_peers do
      local peer = peers[i]
      local az_index = peer.weight
      if peer.down then
        result[peer.name] = 0
      else
        if az_index == current_az_index or current_zone.nb_peers == 0 then
          result[peer.name] = current_zone.pweight / weight_gcd
        else
          result[peer.name] = az_sweight[az_index] / weight_gcd
        end
      end
    end
  end

  return result
end

function gen_json_peers_status_info(peers, servers_list, bits, idx)
  local npeers = #peers
  local weight_sum = 1

  if (servers_list and npeers > 0) then
    weight_sum = 0
    for _, weight in pairs(servers_list) do
      weight_sum = weight_sum + weight
    end
  end

  for i = 1, npeers do
    local peer = peers[i]
    local status = peer.down and "down" or "up"
    local weight = servers_list and servers_list[peer.name] or 1

    bits[idx] = string.format("\"%s\":{\"status\":\"%s\",\"weight\":%s,\"ratio\":%s}%s",
                              peer.name, status, weight, weight * weight_sum, i == npeers and "" or ",")
    idx = idx + 1
  end
  return idx
end

function _M.json_status(servers_list, checkers)
  local upstreams, err = upstream.get_upstreams()
  if not upstreams then
    return "failed to get upstream names: " .. err
  end

  local n = #upstreams
  local bits = {}
  local idx = 1
  bits[idx] = '{'
  idx = idx + 1
  for i = 1, n do
    local up = upstreams[i]
    bits[idx] = string.format("\"%s\":{\"has_checker\":%s,\"primary_peers\":{", up, tostring(checkers[up] or false))
    idx = idx + 1

    local peers, err = upstream.get_primary_peers(up)
    if not peers then
      return "failed to get primary peers in upstream " .. up .. ": "
        .. err
    end
    idx = gen_json_peers_status_info(peers, servers_list[up], bits, idx)

    bits[idx] = string.format("},\"backup_peers\":{")
    idx = idx + 1

    peers, err = upstream.get_backup_peers(up)
    if not peers then
      return "failed to get backup peers in upstream " .. up .. ": "
        .. err
    end
    idx = gen_json_peers_status_info(peers, servers_list[up], bits, idx)
    bits[idx] = "}}"
    bits[idx + 1] = i == n and "" or ","
    idx = idx + 2
  end
  bits[idx] = '}'
  return table.concat(bits)
end

return _M
