local affinity = require('affinity_balancer')
local lu = require('luaunit')

function assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, expected_ratio)
  local az1_weight_sum = 0
  for server, weight in pairs(sl_az1) do
    az1_weight_sum = az1_weight_sum + weight
  end
  local az2_weight_sum = 0
  for server, weight in pairs(sl_az2) do
    az2_weight_sum = az2_weight_sum + weight
  end
  local az3_weight_sum = 0
  for server, weight in pairs(sl_az3) do
    az3_weight_sum = az3_weight_sum + weight
  end

  local servers = {}
  for server, weight in pairs(sl_az1) do
    ratio_sum = sl_az1[server] / az1_weight_sum + sl_az2[server] / az2_weight_sum + sl_az3[server] / az3_weight_sum
    if ratio_sum ~= 0 then
      lu.assertEquals(ratio_sum, expected_ratio)
    end
  end
end

TestUpdateWeight = {} --class
function TestUpdateWeight:setUp()
  self.peers_az1 = {
    [1] = {["name"] = "peer1_az1", ["weight"] = 10, ["down"] = nil},
    [2] = {["name"] = "peer2_az1", ["weight"] = 10, ["down"] = nil},
    [3] = {["name"] = "peer3_az1", ["weight"] = 10, ["down"] = nil},
    [4] = {["name"] = "peer1_az2", ["weight"] = 1, ["down"] = nil},
    [5] = {["name"] = "peer2_az2", ["weight"] = 1, ["down"] = nil},
    [6] = {["name"] = "peer3_az2", ["weight"] = 1, ["down"] = nil},
    [7] = {["name"] = "peer1_az3", ["weight"] = 2, ["down"] = nil},
    [8] = {["name"] = "peer2_az3", ["weight"] = 2, ["down"] = nil},
    [9] = {["name"] = "peer3_az3", ["weight"] = 2, ["down"] = nil},
  }
  self.peers_az2 = {
    [1] = {["name"] = "peer1_az1", ["weight"] = 1, ["down"] = nil},
    [2] = {["name"] = "peer2_az1", ["weight"] = 1, ["down"] = nil},
    [3] = {["name"] = "peer3_az1", ["weight"] = 1, ["down"] = nil},
    [4] = {["name"] = "peer1_az2", ["weight"] = 10, ["down"] = nil},
    [5] = {["name"] = "peer2_az2", ["weight"] = 10, ["down"] = nil},
    [6] = {["name"] = "peer3_az2", ["weight"] = 10, ["down"] = nil},
    [7] = {["name"] = "peer1_az3", ["weight"] = 2, ["down"] = nil},
    [8] = {["name"] = "peer2_az3", ["weight"] = 2, ["down"] = nil},
    [9] = {["name"] = "peer3_az3", ["weight"] = 2, ["down"] = nil},
  }
  self.peers_az3 = {
    [1] = {["name"] = "peer1_az1", ["weight"] = 1, ["down"] = nil},
    [2] = {["name"] = "peer2_az1", ["weight"] = 1, ["down"] = nil},
    [3] = {["name"] = "peer3_az1", ["weight"] = 1, ["down"] = nil},
    [4] = {["name"] = "peer1_az2", ["weight"] = 2, ["down"] = nil},
    [5] = {["name"] = "peer2_az2", ["weight"] = 2, ["down"] = nil},
    [6] = {["name"] = "peer3_az2", ["weight"] = 2, ["down"] = nil},
    [7] = {["name"] = "peer1_az3", ["weight"] = 10, ["down"] = nil},
    [8] = {["name"] = "peer2_az3", ["weight"] = 10, ["down"] = nil},
    [9] = {["name"] = "peer3_az3", ["weight"] = 10, ["down"] = nil},
  }
end

function TestUpdateWeight:testWithOnePeerIsDown()
  self.peers_az1[1].down = true
  self.peers_az2[1].down = true
  self.peers_az3[1].down = true

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 9,
                    ["peer3_az1"] = 9,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, 1/3 + 1/24)
end

function TestUpdateWeight:testWith2PeersAreDownSameAZ()
  self.peers_az1[1].down = true
  self.peers_az2[1].down = true
  self.peers_az3[1].down = true

  self.peers_az1[2].down = true
  self.peers_az2[2].down = true
  self.peers_az3[2].down = true

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 9,
                    ["peer1_az2"] = 2,
                    ["peer2_az2"] = 2,
                    ["peer3_az2"] = 2,
                    ["peer1_az3"] = 2,
                    ["peer2_az3"] = 2,
                    ["peer3_az3"] = 2,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, 1/3 + 2/21)
end

function TestUpdateWeight:testWithOnePeerIsDownIn2AZ()
  self.peers_az1[1].down = true
  self.peers_az2[1].down = true
  self.peers_az3[1].down = true

  self.peers_az1[4].down = true
  self.peers_az2[4].down = true
  self.peers_az3[4].down = true

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 9,
                    ["peer3_az1"] = 9,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 9,
                    ["peer3_az2"] = 9,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, 1/3 + 2/21)
end

function TestUpdateWeight:testWithOnePeerIsDownInAllAZ()
  self.peers_az1[1].down = true
  self.peers_az2[1].down = true
  self.peers_az3[1].down = true

  self.peers_az1[4].down = true
  self.peers_az2[4].down = true
  self.peers_az3[4].down = true

  self.peers_az1[7].down = true
  self.peers_az2[7].down = true
  self.peers_az3[7].down = true

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 1,
                    ["peer3_az1"] = 1,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, 1/2)
end

function TestUpdateWeight:testWithAllPeersOfAnAZAreDown()
  self.peers_az1[1].down = true
  self.peers_az2[1].down = true
  self.peers_az3[1].down = true
  self.peers_az1[2].down = true
  self.peers_az2[2].down = true
  self.peers_az3[2].down = true
  self.peers_az1[3].down = true
  self.peers_az2[3].down = true
  self.peers_az3[3].down = true

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, 1/3 + 1/6)
end

function TestUpdateWeight:testWithAllPeersAreDown()
  self.peers_az1[1].down = true
  self.peers_az2[1].down = true
  self.peers_az3[1].down = true
  self.peers_az1[2].down = true
  self.peers_az2[2].down = true
  self.peers_az3[2].down = true
  self.peers_az1[3].down = true
  self.peers_az2[3].down = true
  self.peers_az3[3].down = true
  self.peers_az1[4].down = true
  self.peers_az2[4].down = true
  self.peers_az3[4].down = true
  self.peers_az1[5].down = true
  self.peers_az2[5].down = true
  self.peers_az3[5].down = true
  self.peers_az1[6].down = true
  self.peers_az2[6].down = true
  self.peers_az3[6].down = true
  self.peers_az1[7].down = true
  self.peers_az2[7].down = true
  self.peers_az3[7].down = true
  self.peers_az1[8].down = true
  self.peers_az2[8].down = true
  self.peers_az3[8].down = true
  self.peers_az1[9].down = true
  self.peers_az2[9].down = true
  self.peers_az3[9].down = true

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 1,
                    ["peer2_az1"] = 1,
                    ["peer3_az1"] = 1,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, 1/3)
end

function TestUpdateWeight:testNoPeerDown()
  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 1,
                    ["peer2_az1"] = 1,
                    ["peer3_az1"] = 1,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  assertAllPeersHaveSameRatio(sl_az1, sl_az2, sl_az3, 1/3)
end

function TestUpdateWeight:testNoPeersDefinedInAZ()
  self.peers_az1[1].weight = 0
  self.peers_az1[2].weight = 0
  self.peers_az1[3].weight = 0

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 1,
                    ["peer2_az1"] = 1,
                    ["peer3_az1"] = 1,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
end

function TestUpdateWeight:testNoPeersDefinedInAZAndOnePeerDown()
  self.peers_az1[1].weight = 0
  self.peers_az1[1].down = true
  self.peers_az1[2].weight = 0
  self.peers_az1[3].weight = 0

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 1,
                    ["peer3_az1"] = 1,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
end

function TestUpdateWeight:testNoPeersDefinedInAzWithAllPeersAreDown()
  self.peers_az1[1].down = true
  self.peers_az1[1].weight = 0
  self.peers_az2[1].down = true
  self.peers_az3[1].down = true
  self.peers_az1[2].down = true
  self.peers_az1[2].weight = 0
  self.peers_az2[2].down = true
  self.peers_az3[2].down = true
  self.peers_az1[3].down = true
  self.peers_az1[3].weight = 0
  self.peers_az2[3].down = true
  self.peers_az3[3].down = true
  self.peers_az1[4].down = true
  self.peers_az2[4].down = true
  self.peers_az3[4].down = true
  self.peers_az1[5].down = true
  self.peers_az2[5].down = true
  self.peers_az3[5].down = true
  self.peers_az1[6].down = true
  self.peers_az2[6].down = true
  self.peers_az3[6].down = true
  self.peers_az1[7].down = true
  self.peers_az2[7].down = true
  self.peers_az3[7].down = true
  self.peers_az1[8].down = true
  self.peers_az2[8].down = true
  self.peers_az3[8].down = true
  self.peers_az1[9].down = true
  self.peers_az2[9].down = true
  self.peers_az3[9].down = true

  local sl_az1 = affinity.update_weights(self.peers_az1)
  local sl_az2 = affinity.update_weights(self.peers_az2)
  local sl_az3 = affinity.update_weights(self.peers_az3)

  lu.assertEquals(sl_az1, {
                    ["peer1_az1"] = 1,
                    ["peer2_az1"] = 1,
                    ["peer3_az1"] = 1,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
  lu.assertEquals(sl_az2, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 1,
                    ["peer2_az2"] = 1,
                    ["peer3_az2"] = 1,
                    ["peer1_az3"] = 0,
                    ["peer2_az3"] = 0,
                    ["peer3_az3"] = 0,
  })
  lu.assertEquals(sl_az3, {
                    ["peer1_az1"] = 0,
                    ["peer2_az1"] = 0,
                    ["peer3_az1"] = 0,
                    ["peer1_az2"] = 0,
                    ["peer2_az2"] = 0,
                    ["peer3_az2"] = 0,
                    ["peer1_az3"] = 1,
                    ["peer2_az3"] = 1,
                    ["peer3_az3"] = 1,
  })
end
-- class TestUpdateWeight

os.exit(lu.LuaUnit:run())
